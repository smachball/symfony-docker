FROM debian

WORKDIR /app
COPY ./symfony /app

# BASE INSTALL
RUN apt -yqq update && apt install -yqq lsb-release ca-certificates apt-transport-https software-properties-common gnupg2 wget bash curl

# INSTALL PHP 8.0 && COMPOSER
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
RUN wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add -
RUN apt -yqq update && apt -yqq install php8.0 php8.0-opcache libapache2-mod-php8.0 php8.0-mysql php8.0-curl php8.0-gd php8.0-intl php8.0-mbstring php8.0-xml php8.0-zip php8.0-fpm php8.0-readline php8.0-pgsql
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

# INSTALL NODEJS && NPM && YARN
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt -yqq update && apt -yqq install nodejs
RUN npm -g install yarn

# INSTALL SYMFONY CLI
RUN echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | tee /etc/apt/sources.list.d/symfony-cli.list
RUN apt -yqq update && apt -yqq install symfony-cli

# CHECK REQUIREMENT SYMFONY
RUN symfony check:requirements

# INSTALL FRAMEWORK
RUN git config --global user.email "you@example.com" && \
git config --global user.name "Your Name"
RUN symfony new ./opt/symfony --webapp

# LAUNCH && EXPOSE
ENTRYPOINT ["/bin/bash", "-c", "composer install && chown -R www-data:www-data var && symfony serve"]
EXPOSE 8000
