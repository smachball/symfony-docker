# HOW TO LAUNCH PROJECT

1. Clone repository, using :

`git clone https://gitlab.com/smachball/symfony-docker.git`

2. Go to symfony repository and copy `.env.example` as `.env`

3. Start Project using docker-compose, using :

```
docker-compose up
```

# HOW TO REBUILD PROJECT

1. Use docker-compose to rebuild project :

```
docker-compose down
docker-compose up --force-recreate --build
```
